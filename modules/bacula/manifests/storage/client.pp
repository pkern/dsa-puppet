# Bacula storage configuration for a client.
#
# This is stored config by a client and then collected on the storage
#
# @param director_server  director for this client
# @param client           name/address of the client (relevant for device names, media type names, etc.)
# @param volume_retention_full  how long to keep volumes with full backups
# @param volume_retention_diff  how long to keep volumes with differential backups
# @param volume_retention_inc   how long to keep volumes with incremental backups
define bacula::storage::client(
  String $director_server,
  Stdlib::Host $client = $name,
  String $volume_retention_full = '100 days', # XXX remove defaults
  String $volume_retention_diff = '50 days', # XXX remove defaults
  String $volume_retention_inc  = '30 days', # XXX remove defaults
) {
  include bacula::storage

  $device_name     = "${bacula::storage::filestor_device}-${client}"
  $media_type_name = "${bacula::storage::filestor_name}-${client}"
  $directory       = "${bacula::storage::backup_path}/${client}"

  # this is created in both bacula::storage::client and
  # bacula::storage::director and needs to be the same
  $dir_storage_secret = hkdf('/etc/puppet/secret', "bacula::director<->storage::${director_server}<->${::fqdn}")

  file {
    "/etc/bacula/storage-conf.d/${client}.conf":
      content => template('bacula/storage/sd-per-client.conf.erb'),
      mode    => '0440',
      group   => bacula,
      notify  => Exec['bacula-sd restart-when-idle'],
      ;
    $directory:
      ensure => directory,
      mode   => '0755',
      owner  => bacula,
      group  => bacula,
      ;
  }

  # enable the director to make (client, storage) specific configuration
  @@bacula::director::client_from_storage { $client:
    tag                     => "bacula::to-director::${director_server}",
    client                  => $client,
    storage_address         => $bacula::storage::storage_address,
    port_sd                 => $bacula::storage::port_sd,
    storage_secret          => $dir_storage_secret,
    storage_device_name     => $device_name,
    storage_media_type_name => $media_type_name,
    volume_retention_full   => $volume_retention_full,
    volume_retention_diff   => $volume_retention_diff,
    volume_retention_inc    => $volume_retention_inc,
  }
}
