# our heavy exim class
# @param is_mailrelay this system is a mailrelay, both in and out, for debian hosts
class exim::mx(
  Boolean $is_mailrelay = false,
){
  class { 'exim':
    use_smarthost => false,
    is_mailrelay  => $is_mailrelay,
  }

  include clamav
  include postgrey
  include fail2ban::exim

  package { 'monitoring-plugins-standard':
    ensure => installed,
  }

  ferm::rule::simple { 'dsa-smtp':
    description => 'Allow smtp access from the world',
    port        => '25',
  }
}
