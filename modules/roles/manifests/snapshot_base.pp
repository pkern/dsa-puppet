# just a base class for snapshot things
class roles::snapshot_base {
  ensure_packages ( [
    'build-essential',
    'libssl-dev',
  ], { ensure => 'installed' })
}
