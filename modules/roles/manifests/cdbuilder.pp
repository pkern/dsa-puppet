# basic bits for debian-cd
class roles::cdbuilder {
  # debian-cd wants to make hardlinks to files it doesn't own; let it.
  # make it override protect-links.conf from procps
  base::sysctl { 'cdimage-software-really-needs-to-be-fixed':
    ensure => absent,
  }
  base::sysctl { 'zz-cdimage-software-really-needs-to-be-fixed':
    key   => 'fs.protected_hardlinks',
    value => '0',
  }

  file { '/srv/cdbuilder.debian.org':
    ensure => directory,
    owner  => 'debian-cd',
    group  => 'debian-cd',
    mode   => '2775'
  }
  file { '/home/debian-cd':
    ensure => link,
    target => '/srv/cdbuilder.debian.org',
  }

   ensure_packages ( [
     'debian.org-cdbuilder.debian.org',
   ], { ensure => 'installed' })

  exec { 'add-debian-cd-user-to-kvm':
    command => 'adduser debian-cd kvm',
    onlyif  => "getent group kvm > /dev/null && ! getent group kvm | grep '\\<debian-cd\\>' > /dev/null"
  }
}
