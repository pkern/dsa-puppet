# web service for snapshot.debian.org
#
# Minimal version for the -dev host; still many things todo
class roles::snapshot_web_minimal (
) {
  include apache2
  include apache2::rewrite
  #apache2::module { 'wsgi': }

  #apache2::site { '020-snapshot.debian.org':
  #  site    => 'snapshot.debian.org',
  #  content => template('roles/snapshot/snapshot.debian.org.conf.erb')
  #}
}
