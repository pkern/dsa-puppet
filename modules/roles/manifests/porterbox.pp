# debian.org porterbox
class roles::porterbox {
  include porterbox
  include bacula::not_a_client
}
