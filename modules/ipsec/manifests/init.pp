# basic ipsec configuration
#
# this configures all packages and required kernel modules, but
# doesn't configure any host, see `ipsec::network` instead.
#
# when first loaded, this will add a list of modules to the kernel,
# but this will only load on reboot.
class ipsec {
  package { [
    'strongswan',
    'libstrongswan-standard-plugins'
  ]:
    ensure => installed
  }

  service { 'ipsec':
    ensure => running,
  }

  file {
    '/etc/ipsec.conf':
      content => template('ipsec/ipsec.conf.erb'),
      notify  => Service['ipsec'];
    '/etc/ipsec.secrets':
      mode    => '0400',
      content => template('ipsec/ipsec.secrets.erb'),
      notify  => Service['ipsec'];
    '/etc/ipsec.conf.d':
      ensure => 'directory',
      purge   => true,
      force   => true,
      recurse => true,
      mode   => '0755';
    '/etc/ipsec.secrets.d':
      ensure => 'directory',
      purge   => true,
      force   => true,
      recurse => true,
      mode   => '0700';
    '/etc/ipsec.conf.d/00-default.conf':
      content => template('ipsec/ipsec.conf-00-default.conf.erb'),
      notify  => Service['ipsec'];
    '/etc/strongswan.d/charon-logging.conf':
      content => template('ipsec/strongswan-charon-logging.conf'),
      notify  => Service['ipsec'];
  }

  ferm::rule {
    'ipsec-peers':
      description => 'ipsec protocols are allowed from the ipsec peers',
      domain      => '(ip ip6)',
      chain       => 'ipsec-peers',
      rule        => 'DROP',
      prio        => 'zzz-999';
    'ipsec':
      description => 'ipsec protocols are allowed from the ipsec peers',
      domain      => '(ip ip6)',
      rule        => @(EOF),
                     proto udp dport (isakmp 4500) jump ipsec-peers;
                     proto esp                     jump ipsec-peers
                     | EOF
  }

  # Since we disable module loading after boot, we want to load them all at boot time
  $modules = split('af_alg af_key ah4 algif_skcipher ansi_cprng authenc drbg echainiv esp4 ipcomp macvlan macvtap tunnel4 vhost vhost_net xfrm4_mode_tunnel xfrm4_tunnel xfrm6_mode_tunnel xfrm_algo xfrm_ipcomp xfrm_user', ' ') # lint:ignore:140chars
  base::linux_module { $modules: }
}
