#!/usr/bin/perl
#
# Plugin to monitor more from apache than you could ever imagine is there
#
# Copyright (C) 2006 Joerg Jaspert, based on a script for rrd, which is
# Copyright (C) 2000  Andrew Williams
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; only version 2.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


# Requirements:
#       - Needs access to http://localhost/server-status (or modify the
#         address for another host). See your apache documentation on how to
#         set up this url in your httpd.conf. Apache needs ExtendedStatus
#         enabled for this plugin to work
#
# Tip: To see if it's already set up correctly, just run this plugin
# with the parameter "autoconf". If you get a "yes", everything should
# work like a charm already.
#
# Parameters supported:
#
#       config
#       autoconf
#
# Configurable variables
#
#       url      - Override default status-url
#       ports    - HTTP port numbers
# Magic markers:
#%# family=auto
#%# capabilities=autoconf

use strict;
use warnings;
use Munin::Plugin;

my $ret = undef;

if (! eval "require LWP::UserAgent;")
{
        $ret = "LWP::UserAgent not found";
}

if (! eval "require FileHandle;")
{
        $ret = "Perlmodule Filehandle not found";
}

my $URL = exists $ENV{'url'} ? $ENV{'url'} : "http://127.0.0.1:80/server-status";
if ( exists $ARGV[0] and $ARGV[0] eq "autoconf" )
{
        if ($ret)
        {
                print "no ($ret)\n";
                exit 1;
        }

        my $ua = LWP::UserAgent->new(timeout => 30);

                my $response = $ua->request(HTTP::Request->new('GET',$URL . "?auto"));
                if ($response->is_success and $response->content =~ /^Total Accesses:/im) {
                  print "yes\n";
                  exit 0;
        } else {
                  print "no\n";
                  print $response->content;
                  exit 1;
                }
}

if ( exists $ARGV[0] and $ARGV[0] eq "config" )
{
        print "graph_title Apache details\n";
        print "graph_args --base 1000 -l 0\n";
        print "graph_vlabel processes\n";
        print "graph_category apache\n";
                print "graph_info This graphs shows details about apache\n";
                print "idleservers.label Idle Servers\n";
                print "activeservers.label Active Servers\n";
                print "waiting.label Waiting\n";
                print "starting.label Starting\n";
                print "reading.label Reading\n";
                print "writing.label Writing\n";
                print "keepalive.label Keepalive\n";
                print "dns.label DNS lookup\n";
                print "logging.label Logging\n";
                print "graceful.label Graceful\n";
                exit 0;
}

my $ua = LWP::UserAgent->new(timeout => 30,
                             agent => sprintf("munin/%s (libwww-perl/%s)", $Munin::Common::Defaults::MUNIN_VERSION, $LWP::VERSION));

my $response = $ua->request(HTTP::Request->new('GET', $URL . "?auto" ));

#
# Collect the results into a hash table
my @values = grep /:\s/, split /\n/, $response->content;
my %ans = (split/:\s|\n/, join("\n", @values));
#warn "unexpected (non-server-status) data seen:\n".$response->content."\n" unless keys %ans == 10;

my $Waiting    = $ans{Scoreboard} =~ tr/_/_/;
my $Starting   = $ans{Scoreboard} =~ tr/S/S/;
my $Reading    = $ans{Scoreboard} =~ tr/R/R/;
my $Writing    = $ans{Scoreboard} =~ tr/W/W/;
my $Keepalive  = $ans{Scoreboard} =~ tr/K/K/;
my $DNS        = $ans{Scoreboard} =~ tr/D/D/;
my $Logging    = $ans{Scoreboard} =~ tr/L/L/;
my $Graceful   = $ans{Scoreboard} =~ tr/G/G/;


print "idleservers.value $ans{IdleWorkers}\n";
print "activeservers.value $ans{BusyWorkers}\n";
print "waiting.value $Waiting\n";
print "starting.value $Starting\n";
print "reading.value $Reading\n";
print "writing.value $Writing\n";
print "keepalive.value $Keepalive\n";
print "logging.value $Logging\n";
print "dns.value $DNS\n";
print "graceful.value $Graceful\n";

