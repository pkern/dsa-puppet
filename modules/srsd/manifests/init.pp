# manage an srsd installation
class srsd {

  $service_file = "/etc/systemd/system/srsd.service"
  
  $srsd_secret = hkdf('/etc/puppet/secret', "srsd-${::fqdn}")

  package { 'srs':
    ensure => installed,
  }

  group { '_srsd':
    ensure => 'present',
    system => true,
  }

  user { '_srsd':
    ensure => 'present',
    system => true,
    gid    => '_srsd',
    home   => '/etc/srsd',
    shell  => '/bin/false',
  }

  file { '/etc/srsd':
    ensure => 'directory',
    mode   => '0700',
    owner  => '_srsd',
    group  => '_srsd',
  }

  file { '/etc/srsd/secret':
    ensure  => present,
    content => $srsd_secret,
    mode    => '0440',
    group   => '_srsd',
  }

  file { [ '/etc/srsd/perl',
           '/etc/srsd/perl/Mail',
           '/etc/srsd/perl/Mail/SRS',
         ]:
    ensure => 'directory',
    mode   => '0700',
    owner  => '_srsd',
    group  => '_srsd',
  }
  file { '/etc/srsd/perl/Mail/SRS/Daemon.pm':
    source => 'puppet:///modules/srsd/Daemon.pm',
    notify  => Service['srsd'],
  }

  file { $service_file:
    ensure  => present,
    content => template('srsd/srsd.service.erb'),
    notify  => Exec['systemctl daemon-reload'],
  }
  service { "srsd":
    ensure    => running,
    enable    => true,
    notify    => Exec['systemctl daemon-reload'],
    subscribe => [ File[$service_file], ],
  }
}
