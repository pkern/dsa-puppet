# Aptitude killer
class buildd::aptitude {
  package { 'python3-psutil':
    ensure => installed,
  }
  file { '/usr/local/sbin/buildd-schroot-aptitude-kill':
    source => 'puppet:///modules/buildd/buildd-schroot-aptitude-kill',
    mode   => '0555',
  }

  concat::fragment { 'puppet-crontab--buildd-aptitude-killer':
    target  => '/etc/cron.d/puppet-crontab',
    content => @(EOF)
      */5 * * * * root /usr/local/sbin/buildd-schroot-aptitude-kill
      | EOF
  }
}
