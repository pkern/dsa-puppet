# pybuildd configuration
class buildd::pybuildd {
  package { 'buildd':
    ensure => purged,
  }

  package { ['python3-retrying', 'python3-yaml']:
    ensure => installed,
  }
  file { '/home/buildd/.forward':
    content  => ":blackhole:\n",
    group   => buildd,
    owner   => buildd,
  }
  file { '/home/buildd/.config/systemd/user':
    ensure  => link,
    target  => '../../pybuildd/systemd/user',
  }
  file { '/home/buildd/.config/user-tmpfiles.d':
    ensure  => link,
    target  => '../pybuildd/systemd/user-tmpfiles.d',
  }
  dsa_systemd::linger { 'buildd': }
  file { '/etc/systemd/journald.conf.d':
    ensure  => directory,
    mode    => '755',
  }
  file { '/etc/systemd/journald.conf.d/persistency.conf':
    source => 'puppet:///modules/dsa_systemd/persistency.conf',
  }

  # Make sure that the build directory have the correct permissions.
  # This should go away once pybuildd issue #3 is solved.
  file { '/home/buildd/build':
    ensure  => directory,
    mode    => '2750',
    group   => buildd,
    owner   => buildd,
  }
}
