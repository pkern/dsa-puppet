# A base class for staticsync.  This owns the configuration variables, which
# should be set by hiera.
class staticsync (
  String $user,
  String $basedir,
  # for ssh/firewalling purposes
  Array[Stdlib::IP::Address] $public_addresses = $base::public_addresses,

  Hash $components = {},
  Hash $mirror_overrides = {},
) {
}
