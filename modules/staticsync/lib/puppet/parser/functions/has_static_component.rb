module Puppet::Parser::Functions
  newfunction(:has_static_component, :type => :rvalue) do |args|
      static_component = args[0]
      fqdn = lookupvar('fqdn')

      mirror_overrides = lookupvar('staticsync::mirror_overrides')
      if mirror_overrides
        if mirror_overrides.include?(fqdn)
          if mirror_overrides[fqdn].include?('components-include')
            if mirror_overrides[fqdn]['components-include'].include?(static_component)
              return true
            else
              return false
            end
          end
        end
      end

      components = lookupvar('staticsync::components')
      if components
        if components.include?(static_component)
          if components[static_component].include?('limit-mirrors')
            return (components[static_component]['limit-mirrors'].include?(fqdn))
          elsif components[static_component].include?('exclude-mirrors')
            return (not components[static_component]['exclude-mirrors'].include?(fqdn))
          else
            return true
          end
        end
      end

      err "Static component #{static_component} appears to be not defined"
      return false
  end
end
