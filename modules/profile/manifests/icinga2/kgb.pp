# Set up the kgbbot and client for notification purposes
class profile::icinga2::kgb (
) {
  package{ 'kgb-client':
    ensure =>  installed,
  }

  file { '/opt/monitoring/etc/notification_kgb.conf':
    owner => 'root',
    group => 'nagios',
    mode  => '0440',
  }

  icinga2::object::notificationcommand{ 'notification_kgb' :
    command   => ['/opt/monitoring/bin/notification_kgb'],
    arguments => {
      '--host-name'      => {
        required => true,
        value    => '$host.name$',
      },
      '--host-state'     => {
        value => '$host.state$',
      },
      '--host-output'    => {
        value => '$host.output$',
      },
      '--service-name'   => {
        value => '$service.name$',
      },
      '--service-state'  => {
        value => '$service.state$',
      },
      '--service-output' => {
        value => '$service.output$',
      },
      #"-t" = {
      #  required = true
      #  value = "$notification.type$"
      #}
      #"-d" = {
      #  required = true
      #  value = "$icinga.long_date_time$"
      #}
    },
    target    => '/etc/icinga2/conf.d/notificationcommands.conf',
  }
  mon::install_command { 'notification_kgb':
    include_check_class => false,
  }

  icinga2::object::user { 'kgb-bot':
    target       => '/etc/icinga2/conf.d/users.conf',
  }

  ['Host', 'Service'].each |$apply_target| {
    icinga2::object::notification { "kgb-notification-${apply_target}":
      target       => '/etc/icinga2/conf.d/notifications-kgb.conf',
      apply        => true,
      apply_target => $apply_target,
      interval     => '24h',
      users        => ['kgb-bot'],
      assign       => ['true'],
      command      => 'notification_kgb',
      types        => ['Recovery', 'Problem'],
    }
  }
}
