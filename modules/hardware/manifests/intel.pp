class hardware::intel {
	if $::virtual == 'physical' and $::debarchitecture == 'amd64' and $::processor0 =~ /^Intel/ {
		package { 'iucode-tool':
			ensure => installed,
		}
		package { 'intel-microcode':
			ensure => installed,
		}
	}
}
