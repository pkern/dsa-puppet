class hardware::amd {
	if $::virtual == 'physical' and $::debarchitecture == 'amd64' and $::processor0 =~ /^AMD/ {
		package { 'amd64-microcode':
			ensure => installed,
		}
	}
}
