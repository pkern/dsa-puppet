# declare a systemd service override
# @param content  content of systemd override file
# @param source   source of systemd override file content
# @param ensure   present or avsent
define dsa_systemd::override (
  Optional[String] $source = undef,
  Optional[String] $content = undef,
  Enum['present','absent'] $ensure = 'present',
) {
  $dir = "/etc/systemd/system/${name}.service.d"
  $dest = "${dir}/override.conf"


  if defined(Service[$name]) {
    $notify = [ Exec['systemctl daemon-reload'], Service[$name] ]
  } else {
    $notify = [ Exec['systemctl daemon-reload'] ]
  }

  $directory_ensure = $ensure ? { 'present' => 'directory', 'absent' => 'absent' }
  file { $dir:
    ensure => $directory_ensure,
    mode   => '0755',
    force  => true,
  }
  file { $dest:
    ensure  => $ensure,
    source  => $source,
    content => $content,
    notify  => $notify,
  }
}
