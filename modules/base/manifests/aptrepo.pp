# Add an apt repository to sources.list.d
#
# @param url
#   Base URL of the apt source
# @param suite
#   suite(s) of the apt source
# @param components
#   List of components
# @param key
#   Optional puppet source to a pgp key that signs this repository
# @param ensure
#   Whether to install or remove this apt source
define base::aptrepo (
  Variant[String,Array[String]] $url,
  Variant[String,Array[String]] $suite,
  Variant[String,Array[String]] $components,
  Optional[String] $key = undef,
  Enum['present', 'absent'] $ensure = present
) {
  include base::aptkeydir

  if $key {
    file { "/etc/apt/trusted.gpg.d/${name}.gpg":
      ensure => absent,
    }

    file { "/etc/apt/puppet-keys.d/${name}.gpg":
      ensure => $ensure,
      source => $key,
      mode   => '0664',
    }
  }

  file { "/etc/apt/sources.list.d/${name}.list":
    ensure  => $ensure,
    content => template('base/aptrepo.erb'),
    notify  => Exec['apt-get update'],
  }
}
