class rsync {

	package { 'rsync':
		ensure => installed,
	}

	service { 'rsync':
		ensure  => stopped,
		require => Package['rsync'],
	}

	file { '/etc/logrotate.d/dsa-rsyncd':
		source  => 'puppet:///modules/rsync/logrotate.d-dsa-rsyncd',
		require => Package['debian.org'],
	}
	file { '/var/log/rsyncd':
		ensure => directory,
		mode   => '0755',
	}

	ferm::rule { 'dsa-rsync':
		domain      => '(ip ip6)',
		description => 'Allow rsync access',
		rule        => '&SERVICE(tcp, 873)'
	}
	ferm::rule { 'dsa-rsync-connlimit':
		domain      => '(ip ip6)',
		description => 'rate limit for rsync',
		prio        => '005',
		rule        => 'proto tcp mod state state (NEW) interface !lo dport 873 mod connlimit connlimit-above 5 DROP',
	}

	file { '/usr/local/sbin/systemd-cleanup-failed-rsyncs':
		ensure => absent,
	}
}
