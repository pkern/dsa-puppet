# NFS server checks
#
# @param expect_autofs whether to check for the existance or the non-existances of autofs
class debmon::check_autofs (
  Optional[Boolean] $expect_autofs = undef,
) {
  if pick($expect_autofs, defined(Class['autofs::common'])) {
    mon::service { 'procs/automount':
      vars => {
        argument => '/usr/sbin/automount',
        command  => 'automount',
        user     => 'root',
        warning  => '1:1',
        critical => '1:',
      }
    }
  } else {
    # unwanted processes
    [
      'automount',
    ].each |$command| {
      mon::service { "procs/unwanted-process-${command}":
        check_interval => '60m',
        retry_interval => '15m',
        vars           => {
          command => $command,
          warning => '0:0'
        },
      }
    }
  }
}
# vim:set fdm=marker:
