class entropykey {

	if getfromhash($deprecated::nodeinfo, 'entropy_key') {
		include entropykey::provider
	}

	$entropy_provider  = entropy_provider($::fqdn, $deprecated::nodeinfo)
	case $entropy_provider {
		false:   {}
		local:   { include entropykey::local_consumer }
		default: {
			class { 'entropykey::remote_consumer':
				entropy_provider => $entropy_provider,
			}
		}
	}

}
