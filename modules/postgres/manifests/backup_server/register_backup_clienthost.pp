# register this host at the backup servers
#
# This class set up the ssh authorization on the backup servers
# so this client can push WAL segments.  Furthermore, the
# client will be allowed to read other hosts backups -- specify
# the list of allowed target hosts via params.
#
# @param allow_read_basedir  directory under which files can be read
# @param allow_read_hosts    subdirectories under base to allow
class postgres::backup_server::register_backup_clienthost (
  String $allow_read_basedir = '/srv/backups/pg',
  Array[Stdlib::Fqdn] $allow_read_hosts = [],
) {
  include postgres::backup_server::globals

  $allowstr = $allow_read_hosts.map |$host| { "--read-allow=${allow_read_basedir}/${host}" }.join(' ')
  $ssh_command = "/usr/local/bin/debbackup-ssh-wrap ${allowstr} ${::hostname}"

  ssh::authorized_key_add { 'register_backup_clienthost':
    target_user => $postgres::backup_server::globals::backup_unix_user,
    key         => dig($facts, 'ssh_keys_users', 'postgres', 'id_rsa.pub', 'line'),
    command     => $ssh_command,
    from        => $base::public_addresses,
    collect_tag => $postgres::backup_server::globals::tag_source_sshkey,
  }
}
